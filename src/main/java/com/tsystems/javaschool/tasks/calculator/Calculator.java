package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {
    private Stack<Double> numbers = new Stack<>();
    private Stack<String> operations = new Stack<>();

    private int getPriority(String s){
        if (s.matches("[*/]"))
            return 3;
        if (s.matches("[\\-+]"))
            return 2;
        if (s.matches("\\p{Ps}"))
            return 1;
        else
            return 0;
    }

    private List<String> getTokensFromString(String expression) {
        if (expression == null || expression.equals(""))
            return null;
        List<String> tokens = new ArrayList<>();
        String checkLAndP = "([\\p{L}[\\p{P}&&[^\\p{Ps}]&&[^\\p{Pe}]&&[^.*/\\-]]])";
        String checkRepeats = "([\\p{P}&&[^\\p{Ps}]&&[^\\p{Pe}]]{2})|(\\+{2})";
        String checkDotsPlacement = "(\\.[\\D]+)|(\\.+\\z)|(\\A\\.)|([\\D]+\\.)|(\\d+\\.\\d+\\.)";
        Pattern patternToCheck = Pattern.compile(checkDotsPlacement + "|" + checkRepeats + "|" + checkLAndP);
        Pattern patternToAdd = Pattern.compile("([0-9]+\\.?[0-9]*)|(\\p{Pe}|\\p{Ps}|\\+|-|\\*|/|)");
        Matcher matcherToCheck = patternToCheck.matcher(expression);
        Matcher matcherToAdd = patternToAdd.matcher(expression);
        if (matcherToCheck.find())
            return null;
        else {
            while (matcherToAdd.find()) {
                tokens.add(matcherToAdd.group());
            }
        }
        int numberOfLeftParenthesis = 0;
        int numberOfRightParenthesis = 0;
        char[] arr = expression.toCharArray();
        for (char x : arr) {
            if (x == 40)
                numberOfLeftParenthesis++;
            if (x == 41)
                numberOfRightParenthesis++;
        }
        if (numberOfLeftParenthesis != numberOfRightParenthesis)
            return null;
        return tokens;
    }

    private double getResultFromOperation(String operation, double firstNumber, double secondNumber){
        double result = 0;
        switch (operation){
            case "/":
                result = secondNumber/firstNumber;
                break;
            case "*":
                result = secondNumber*firstNumber;
                break;
            case "-":
                result = secondNumber-firstNumber;
                break;
            case "+":
                result = secondNumber+firstNumber;
                break;
        }
        return result;
    }

    private void compute(String operation){
        double firstNumber = numbers.pop();
        double secondNumber = numbers.pop();
        double resultFromOperation = getResultFromOperation(operation, firstNumber, secondNumber);
        numbers.push(resultFromOperation);
    }

    private String getAnswer(double inputAnswer){
        if (String.valueOf(inputAnswer).equals("NaN") || String.valueOf(inputAnswer).equals("Infinity"))
            return null;
        int intInputAnswer = (int) Math.round(inputAnswer);
        if (intInputAnswer == inputAnswer)
            return String.valueOf(intInputAnswer);
        else {
            if (BigDecimal.valueOf(inputAnswer).scale() > 4)
                return String.format("%.4f", inputAnswer);
        }
        return String.valueOf(inputAnswer);
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        String currentComponent;
        int currentPriority;
        String operation;
        double uncheckedAnswer;
        List<String> tokens = getTokensFromString(statement);
        if (tokens == null)
            return null;
        for (int j = 0; j < tokens.size(); j++){
            currentComponent = tokens.get(j);
            if (currentComponent.equals("")){
                while (true) {
                    if (operations.empty())
                        break;
                    operation = operations.pop();
                    compute(operation);
                }
                break;
            }
            if (currentComponent.matches("[0-9]+[.,]?[0-9]*")) {
                numbers.push(Double.parseDouble(currentComponent));
            }
            else {
                currentPriority = getPriority(currentComponent);
                if (currentPriority == 0){
                    while (true) {
                        if (operations.isEmpty() || getPriority(operations.peek()) == 0)
                            return null;
                        operation = operations.pop();
                        if (getPriority(operation) == 1)
                            break;
                        compute(operation);
                    }
                }
                else if (operations.empty() || getPriority(operations.peek()) < currentPriority || currentPriority == 1) {
                    operations.push(currentComponent);
                }
                else {
                    while (getPriority(operations.peek()) >= currentPriority) {
                        operation = operations.pop();
                        compute(operation);
                        if (operations.empty())
                            break;
                    }
                    operations.push(currentComponent);
                }
            }
        }
        uncheckedAnswer = numbers.pop();
        return getAnswer(uncheckedAnswer);
    }
}