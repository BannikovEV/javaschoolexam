package com.tsystems.javaschool.tasks.subsequence;



import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null)
            throw new IllegalArgumentException();
        if (x.equals(y))
            return true;
        if (x.size() > y.size())
            return false;
        @SuppressWarnings("unchecked")
        List yNewArray = new ArrayList(y);
        int numberOfFound = 0;
        for (Object i : x){
            while (yNewArray.size() >= x.size()) {
                if (!i.equals(yNewArray.get(numberOfFound)))
                    yNewArray.remove(numberOfFound);
                else {
                    numberOfFound++;
                    break;
                }
            }
        }
        return numberOfFound == x.size();
    }
}
