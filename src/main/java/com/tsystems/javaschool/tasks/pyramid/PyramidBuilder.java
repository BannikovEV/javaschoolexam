package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers){
        List<Integer> numbers;
        try{
            numbers = new ArrayList<>(inputNumbers);
            Collections.sort(numbers);
        }
        catch (Throwable e){
            throw new CannotBuildPyramidException();
        }
        int numberOfColumns = (int) Math.sqrt((double)2*numbers.size());
        if (numberOfColumns*(numberOfColumns+1)/2 != numbers.size())
            throw new CannotBuildPyramidException();
        int[][] arr = new int[numberOfColumns][2*numberOfColumns - 1];
        while (!numbers.isEmpty()) {
            for (int i = 1; i <= numberOfColumns; i++) {
                int numberOfZeros = numberOfColumns - i;
                int j = 0;
                for (int k = 1; k < (2 * i); k++) {
                    if (numberOfColumns == i && j == 0 || arr[i-1][numberOfZeros + j - 1] == 0){
                        arr[i-1][numberOfZeros + j] = numbers.get(0);
                        numbers.remove(0);
                    }
                    j++;
                }
            }
        }
        return arr;
    }

}
